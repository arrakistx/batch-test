#batch-test
conda create --name batch-test python=3.6 
conda install --name batch-test -c bioconda -c conda-forge snakemake
conda install --name batch-test -c bioconda snakemake trimmomatic
conda install --name batch-test pandas pyyaml

