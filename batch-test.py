#!/usr/bin/env python3

""" 
Pared-down test pipeline for Snakemake / AWS batch

"""

__pipeline__ = 'batch-test'
__version__ = '0.1' 
__author__ = 'Lee Vandivier'
# __license__ = 'GPLv3'

# Load standard modules

import argparse
import sys
import re
from glob import glob
from os import path, makedirs, symlink, environ
from multiprocessing import cpu_count
from subprocess import Popen, PIPE
from time import sleep

# Load non-standard modules

try:
    import json
    import pandas as pd
    from snakemake import snakemake


except ModuleNotFoundError as e:
    print(e)
    print('===========')
    print('Failed to find a required module. Did you forget to activate the "batch-test" environment?')
    print('Try:\n source activate batch-test')
    exit()
    

# Get location of pipeline directory

pipeline_dir = path.dirname(path.realpath(__file__))

# Check that batch-test is running in correct environment

if 'CONDA_DEFAULT_ENV' in environ:
    if environ['CONDA_DEFAULT_ENV'] != 'batch-test':
        print('Warning: batch-test is running in Conda environment "{}"'.format(environ['CONDA_DEFAULT_ENV']))
        print('We recommend creating a dedicated batch-test environment called "batch-test".')
        print('===========')
        sleep(2)


###################
# Parse arguments #
###################

parser = argparse.ArgumentParser(description="Batch test", formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('input_dir', default = '0_raw', help='Directory where input FASTQ files are found')
parser.add_argument('output_dir', default = '1_trimmomatic', help='Directory where output files will be created')
parser.add_argument('--input_sheet', default = 'input_sheet.csv', help='Input spreadsheet, if not found in standard location')
parser.add_argument('--num_cores', help='Number of cores to use (default: all available)', type=int, default=cpu_count())

#Snakemake generic parameters
parser.add_argument('--overwrite',
                    help='Overwrite final output files even if precurors unchanged.', action='store_true')
parser.add_argument('--dryrun', help='Snakemake dry run', action='store_true') 
parser.add_argument('--force_incomplete', help='Snakemake force overwrite incomplete files', action='store_true') 
parser.add_argument('--unlock', help='Snakemake unlock directory only', action='store_true') 
parser.add_argument('--unique', help='retain only uniquely mapping reads (map q > 10)', action='store_true')
parser.add_argument('--processors', help="Maximum number of processors to use when multiprocessing is available", type=int, default=1)

args = parser.parse_args()

if not path.isdir(args.input_dir):
    raise Exception('Working directory {} not found.'.format(args.input_dir))

# Perform basic validation on files

if not path.isfile(args.input_sheet):
    raise Exception('Input spreadsheet not found at: {}'.format(args.input_sheet))

if args.input_sheet.endswith('.xlsx') or args.input_sheet.endswith('.xls'):
    sample_df = pd.read_excel(args.input_sheet)

elif args.input_sheet.endswith('.csv'):
    sample_df = pd.read_csv(args.input_sheet)

if len(sample_df) == 0:
    raise Exception('Input spreadsheet is empty or corrupted.')


###############################
# Verify presence of raw data #
###############################

fastq_dict = {}

def validate_sample_name(sample_name):
    if '_' in sample_name:
        raise Exception('For now, batch-test does not support underscores in sample names.\
            Please replace them in sample {}.'.format(sample_name))
    if sample_name != '*':
        if not re.match(r'^[a-zA-Z0-9][A-Za-z0-9-]*$', sample_name):
            print('-- Sample names should only include alphanumeric characters and dashes, or a single "*". --')
            raise Exception('Problematic sample name: {}'.format(sample_name))

def parse_sample_metadata(row, sample_name, fastq_dict):

    ### parse fastqs associations ###
    all_fastq_files = sorted(glob(path.join(args.input_dir, '**/*{}_*.fastq.gz'.format(sample_name)), recursive=True))

    # Try to match by sample name
    matching_fastq_files = [_ for _ in all_fastq_files if re.search('[/_]' + sample_name, _)]
    
    # If that fails, try to use the sample name as a potential sample number
    if len(matching_fastq_files) == 0:
        matching_fastq_files = [_ for _ in all_fastq_files if re.search('_' + sample_name + '_', _)]

    if len(matching_fastq_files) != 2:
        print(' '.join(matching_fastq_files))
        print(row)
        raise Exception('Wrong number of matching FASTQ files for sample.')

    assert '_R1' in matching_fastq_files[0]
    assert '_R2' in matching_fastq_files[1]
    assert matching_fastq_files[0].replace('_R1', '_R0') == matching_fastq_files[1].replace('_R2', '_R0')

    fastq_dict[sample_name] = tuple(matching_fastq_files) 

for _, row in sample_df.iterrows():
    sample_name = row['Sample']
    validate_sample_name(sample_name)
    parse_sample_metadata(row, sample_name, fastq_dict,)
  
sample_names = list(fastq_dict.keys())
config_dict = {'sample_names':sample_names, 'processors': args.processors}


# Create a folder with symlinks to FASTQ files in a way that they are now named consistently with sample names. Also symlink to reference files

if not path.isdir(args.output_dir):
    makedirs(args.output_dir)

sanitized_fastq_dir = path.join(args.output_dir, 'sanitized')
if not path.isdir(sanitized_fastq_dir):
    makedirs(sanitized_fastq_dir)

for sample_name in fastq_dict:
    r1_file, r2_file = fastq_dict[sample_name]
    
    new_r1_file = path.join(sanitized_fastq_dir, '{}_R1.fastq.gz'.format(sample_name))
    new_r2_file = path.join(sanitized_fastq_dir, '{}_R2.fastq.gz'.format(sample_name))

    if not path.isfile(new_r1_file):
        symlink(path.abspath(r1_file), new_r1_file)

    if not path.isfile(new_r2_file):
        symlink(path.abspath(r2_file), new_r2_file)

    fastq_dict[sample_name] = (new_r1_file, new_r2_file)

################################
# Run analysis using Snakemake #
################################

# Define per-sample targets and generate prerequisite files using Snakemake

all_targets = ['aggregated_reads/aggregate_R1.fastq.gz', 'aggregated_reads/aggregate_R2.fastq.gz']

snakemake(path.join(pipeline_dir, 'src/Snakefile'), targets=all_targets, forcetargets=args.overwrite,
          workdir=args.output_dir, cores=args.num_cores, dryrun=args.dryrun, force_incomplete = args.force_incomplete, unlock = args.unlock, config=config_dict)
